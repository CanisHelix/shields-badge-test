### Test Repository

This repository exist to serve as a Gitea Test repository for shields.io

## Badges

![Gitea Release](https://img.shields.io/gitea/v/release/CanisHelix/shields-badge-test?gitea_url=https%3A%2F%2Fcodeberg.org)
![Gitea language count](https://img.shields.io/gitea/languages/count/CanisHelix/shields-badge-test?gitea_url=https%3A%2F%2Fcodeberg.org)